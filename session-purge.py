#!/usr/bin/env python

import boto.dynamodb
import time
from optparse import OptionParser
import sys


###
#  Deletes expired sessions from dynamodb.
#  AWS creds are read from ~/.boto
###

def main():
  parser = OptionParser()
  parser.add_option(
    '--ec2-region', 
    action='store', 
    type='string', 
    dest='ec2_region', 
    default='ap-southeast-1', 
    help='ec2 region, default = ap-southeast-1')

  parser.add_option(
    '--max-retries', 
    action='store', 
    type='int', 
    dest='max_retries', 
    default=4,
    help='max times to retry on throttle errors, default = 4')

  parser.add_option(
    '--dryrun', 
    action='store_true', 
    dest='dryrun', 
    default=False, 
    help='dryrun only (no actual deletes), default = False')
  
  (options, args)= parser.parse_args()

  conn = boto.dynamodb.connect_to_region(options.ec2_region)
  sessions_table = conn.get_table('sessions')
  sessions = sessions_table.scan()

  now = int(time.time())
  delete_count = 0
  total_count = 0
  attempt = 0

  for session in sessions:
    try:
        total_count = total_count + 1
        expiration = session['expires']
        if int(expiration) < now:
          if options.dryrun is False:
            session.delete()
          delete_count = delete_count + 1
    except DynamoDBThroughputExceededError:
      time.sleep( 2 ^ attempt ) 
      attempt = attempt + 1
      if attempt > options.max_retires:
        print 'Max retries exceeded'
        sys.exit(1)
      continue
     
  end = int(time.time())

  print "Deleted %s/%s sessions in %s seconds.  Dryrun=%s" % (delete_count, total_count, (end - now), options.dryrun)

if __name__ == "__main__":
   main()
